<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// VIEWS

// HOME
Route::get('/size', function () {
    return view('size');
});

// HOME
Route::get('/', function () {
    return view('home');
});

// ABOUT
Route::get('/about', function () {
    return view('about');
});

Route::get('areasofpractice', function () {
    return redirect('areas-of-practice');
});

// AREAS OF PRACTICE
Route::get('/areas-of-practice', function () {
    return view('areasofpractice');
});



// STRATEGY
Route::get('/strategy', function () {
    return view('strategy');
});

// TALENT MANAGEMENT
Route::get('/talent-management-architecture', function () {
    return view('talentmanagement');
});

Route::get('talentmanagement', function () {
    return redirect('talent-management-architecture');
});

// LEADER EFFECTIVENESS
Route::get('/leader-effectiveness', function () {
    return view('leadereffectiveness');
});

Route::get('leadereffectiveness', function () {
    return redirect('leader-effectiveness');
});

// TALENT ANALYTICS
Route::get('/talent-analytics', function () {
    return view('talentanalytics');
});

Route::get('talentanalytics', function () {
    return redirect('talent-analytics');
});


// CLIENTS
Route::get('/clients', function () {
    return view('clients');
});

// CONTACT
Route::get('/contact-us', function () {
    return view('contact');
});

Route::get('contact', function () {
    return redirect('contact-us');
});

// PERFORM SUBMIT CONTACT
Route::post('/submitcontact', [
    'as' => '/submitcontact',
    'uses' => 'LoginController@submitContact'
]);

// CMS
// LOGIN TO CMS
Route::get('/login', function () {
    return view('/cms/login');
});

// CMS HOME
Route::get('/cmshome', function () {

    // IF LOGGED IN
    if (session()->has('loggedIn')) {
        if (session('loggedIn')) {
            return view('/cms/cmshome');
        } else {
            return Redirect('/dologout'); 
        }
    } else {
        return Redirect('/dologout'); 
    } 
});

/* HOME CMS */

// HOME SLIDER IMAGES
Route::get('/homesliderimages', function () {
    return view('cms/homesliderimages');
});

// HOME COPY
Route::get('/homecopy', function () {
    return view('cms/homecopy');
});

/* END HOME CMS */

/* ABOUT CMS */

// ABOUT SLIDER IMAGES
Route::get('/aboutsliderimages', function () {
    return view('cms/aboutsliderimages');
});

// ABOUT BROCHURES
Route::get('/aboutbrochures', function () {
    return view('cms/aboutbrochures');
});

// ABOUT COPY
Route::get('/aboutcopy', function () {
    return view('cms/aboutcopy');
});

/* END ABOUT CMS */

/* AREAS OF PRACTICE CMS */

// AREAS OF PRACTICE SLIDER IMAGES
Route::get('/areasofpracticesliderimages', function () {
    return view('cms/areasofpracticesliderimages');
});

// AREAS OF PRACTICE COPY
Route::get('/areasofpracticecopy', function () {
    return view('cms/areasofpracticecopy');
});

/* END AREAS OF PRACTICE CMS */

/* SOLUTIONS CMS */

// SOLUTIONS SLIDER IMAGES
Route::get('/solutionssliderimages', function () {
    return view('cms/solutionssliderimages');
});

// STRATEGY COPY
Route::get('/strategycopy', function () {
    return view('cms/strategycopy');
});

// STRATEGY CASE STUDIES
Route::get('/strategycasestudies', function () {
    return view('cms/strategycasestudies');
});

// TALENT MANAGEMENT COPY
Route::get('/talentmanagementcopy', function () {
    return view('cms/talentmanagementcopy');
});

// TALENT MANAGEMENT CASE STUDIES
Route::get('/talentmanagementcasestudies', function () {
    return view('cms/talentmanagementcasestudies');
});

// LEADER EFFECTIVENESS COPY
Route::get('/leadereffectivenesscopy', function () {
    return view('cms/leadereffectivenesscopy');
});

// LEADER EFFECTIVENESS CASE STUDIES
Route::get('/leadereffectivenesscasestudies', function () {
    return view('cms/leadereffectivenesscasestudies');
});

// TALENT ANALYTICS COPY
Route::get('/talentanalyticscopy', function () {
    return view('cms/talentanalyticscopy');
});

// TALENT ANALYTICS CASE STUDIES
Route::get('/talentanalyticscasestudies', function () {
    return view('cms/talentanalyticscasestudies');
});

/* END SOLUTIONS CMS */

/* CLIENTS CMS */

// CLIENTS SLIDER IMAGES
Route::get('/clientssliderimages', function () {
    return view('cms/clientssliderimages');
});

// CLIENTS COPY
Route::get('/clientscopy', function () {
    return view('cms/clientscopy');
});

// CLIENTS LIST
Route::get('/clientslist', function () {
    return view('cms/clientslist');
});

/* END CLIENTS CMS */

/* CONTACT CMS */

// CONTACT SLIDER IMAGES
Route::get('/contactsliderimages', function () {
    return view('cms/contactsliderimages');
});

// CONTACT SLIDER IMAGES
Route::get('/contactdetails', function () {
    return view('cms/contactdetails');
});

/* END CONTACT CMS */

// OPERATIONS


// PERFORM LOGIN OPERATION
Route::post('/dologin', 'LoginController@login');

// PERFORM LOGOUT OPERATION
Route::get('/dologout', 'LoginController@logout');

// PERFORM SLIDER IMAGE ADD OPERATION
Route::post('/dosliderimageadd', 'LoginController@doSliderImageAdd');

// PERFORM SLIDER IMAGE DELETE OPERATION
Route::post('/dosliderimagedelete', 'LoginController@doSliderImageDelete');

/* HOME OPS */

// PERFORM HOME COPY EDIT OPERATION
Route::post('/dohomecopyedit', [
    'as' => '/dohomecopyedit',
    'uses' => 'LoginController@doHomeCopyEdit'
]);

/* END HOME OPS */

/* ABOUT OPS */

// PERFORM HOME COPY EDIT OPERATION
Route::post('/doaboutcopyedit', [
    'as' => '/doaboutcopyedit',
    'uses' => 'LoginController@doAboutCopyEdit'
]);

/* END ABOUT OPS */

/* AREAS OF PRACTICE OPS */

// PERFORM AREAS OF PRACTICE COPY EDIT OPERATION
Route::post('/doareasofpracticecopyedit', [
    'as' => '/doareasofpracticecopyedit',
    'uses' => 'LoginController@doAreasOfPracticeCopyEdit'
]);

/* END AREAS OF PRACTICE OPS */

/* SOLUTIONS OPS */

// PERFORM CASE STUDY OPERATION
Route::post('/docasestudyadd', 'LoginController@doCaseStudyAdd');

// PERFORM CASE STUDY DELETE OPERATION
Route::post('/docasestudydelete', 'LoginController@doCaseStudyDelete');

// PERFORM STRATEGY COPY EDIT OPERATION
Route::post('/dostrategycopyedit', [
    'as' => '/dostrategycopyedit',
    'uses' => 'LoginController@doStrategyCopyEdit'
]);

// PERFORM TALENT MANAGEMENT COPY EDIT OPERATION
Route::post('/dotalentmanagementcopyedit', [
    'as' => '/dotalentmanagementcopyedit',
    'uses' => 'LoginController@doTalentManagementCopyEdit'
]);

// PERFORM LEADER EFFECTIVENESS COPY EDIT OPERATION
Route::post('/doleadereffectivenesscopyedit', [
    'as' => '/doleadereffectivenesscopyedit',
    'uses' => 'LoginController@doLeaderEffectivenessCopyEdit'
]);

// PERFORM TALENT ANALYTICS COPY EDIT OPERATION
Route::post('/dotalentanalyticscopyedit', [
    'as' => '/dotalentanalyticscopyedit',
    'uses' => 'LoginController@doTalentAnalyticsCopyEdit'
]);


/* END SOLUTIONS OPS */

/* CLIENTS OPS */

// PERFORM CLIENT ADD OPERATION
Route::post('/doclientadd', 'LoginController@doClientAdd');

// PERFORM CLIENT DELETE OPERATION
Route::post('/doclientdelete', 'LoginController@doClientDelete');

/* END CLIENTS OPS */\

/* CONTACT OPS */

// PERFORM CONTACT DETAILS EDIT OPERATION
Route::post('/docontactdetailsedit', [
    'as' => '/docontactdetailsedit',
    'uses' => 'LoginController@doContactDetailsEdit'
]);

/* END CONTACT OPS */