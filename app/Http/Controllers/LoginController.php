<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use Illuminate\Http\UploadedFile;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\File;
    use Mail;

    class LoginController extends Controller
    {

        // LOGIN TO CMS
        public function login(Request $request)
        {

            // VALIDATE FIELDS
            $validateFields = ([
                'username' => 'required',
                'password' => 'required'
            ]);
            $this->validate($request, $validateFields);

            // GET USERNAME
            $username = $_POST['username'];

            // GET PASSWORD
            $password = $_POST['password'];

            // GET PASS WITH ABOVE USERNAME
            $savedPass = DB::select("SELECT password FROM users where username='$username'");

            // IF NO MATCHES
            if (sizeof($savedPass) == 0) {
                echo "No user found with those credentials.";
            // IF 1 MATCH
            } else if (sizeof($savedPass) == 1) {

                echo "User found";
                
                // CHECK IF HASHED PASSES MATCH
                // PASS MATCH
                if (password_verify($password, $savedPass[0]->password)) {
                    echo "Password valid!";

                    session(['loggedIn' => true]);
                    return redirect("cmshome");
                
                // PASS NOT MATCH
                } else {
                    echo "Password not valid!";
                }

            // IF MULTIPLE MATCHES
            } else if (sizeof($savedPass) > 1) {
                echo "Error. Too many users found. Please contact administrator.";
            } 


        }

        // SUBMIT CONTACT
        public function submitContact(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
                'name' => 'required',
                'email' => 'required',
                'message' => 'required',
                'captcha' => 'required'
            ]);
    
            $this->validate($request, $validateFields);

            $captcha = $request->captcha;

            //CHECK CAPTCHA
            $url = "https://www.google.com/recaptcha/api/siteverify";

            //The data you want to send via POST
            $fields = [
                'secret' => "6LcGQYgUAAAAAEKzgCQNsTutU4VtzzrVK98iUFME",
                'response' => $captcha,
            ];

            //url-ify the data for the POST
            $fields_string = http_build_query($fields);

            //open connection
            $ch = curl_init();

            //set the url, number of POST vars, POST data
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

            //So that curl_exec returns the contents of the cURL; rather than echoing it
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 

            //execute post
            $result = curl_exec($ch);

            if (isset($result)) {

                $name = $request->name;

                $email = $request->email;
    
                $usermessage = $request->message;

            Mail::send('email/contact', ['name' => $name, 'email' => $email, 'usermessage' => $usermessage], function ($message)
            {
                $message->from("info@talentinstitute.com", "Talent Institute");
    
                $message->to("thebadboyfranco@gmail.com")->subject('A new message from Talent Institute Website');
            });
    
            return "sent";

            }

        }

        // ADD SLIDER IMAGE 
        public function doSliderImageAdd(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'image' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif,svg',
            'text' => 'required',
            'type' => 'required'
            ]);

            $this->validate($request, $validateFields);

            $text = $request->text;

            $type = $request->type;

            // GET IMAGE NAME
            $imageName = time() . $request->image->getClientOriginalName();

            switch($type) {
                case 'home':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/home'), $imageName);

                    $newImageURL = '/images/home/' . $imageName;

                    DB::table('homesliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'about':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/about'), $imageName);

                    $newImageURL = '/images/about/' . $imageName;

                    DB::table('aboutsliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'areasofpractice':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/areasofpractice'), $imageName);

                    $newImageURL = '/images/areasofpractice/' . $imageName;

                    DB::table('areasofpracticesliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;
                       

                case 'solutions':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/solutions'), $imageName);

                    $newImageURL = '/images/solutions/' . $imageName;

                    DB::table('solutionssliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'clients':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/clients'), $imageName);

                    $newImageURL = '/images/clients/' . $imageName;

                    DB::table('clientssliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'contact':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/contact'), $imageName);

                    $newImageURL = '/images/contact/' . $imageName;

                    DB::table('contactsliderimages')->insert( [
                        'imagetext' => $text,
                        'imageurl' => $newImageURL]
                    );
                    break;
            }

            return "success";

        }

        // DELETE SLIDER IMAGE
        public function doSliderImageDelete(Request $request) {
 
            // VALIDATE FIELDS
            $validateFields = ([
                'type' => 'required',
                'id' => 'required'
                ]);
    
                $this->validate($request, $validateFields);
    
                $id = $request->id;
    
                $type = $request->type;
    
                switch($type) {
                    case 'home':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('homesliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('homesliderimages')->where('id', '=', $id)->delete();
                        break;

                    case 'about':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('aboutsliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('aboutsliderimages')->where('id', '=', $id)->delete();
                        break;

                    case 'areasofpractice':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('areasofpracticesliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('areasofpracticesliderimages')->where('id', '=', $id)->delete();
                        break;

                    case 'solutions':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('solutionssliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('solutionssliderimages')->where('id', '=', $id)->delete();
                        break;

                    case 'clients':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('clientssliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('clientssliderimages')->where('id', '=', $id)->delete();
                        break;

                    case 'clients':
                        // GET RECORD WITH IMAGE ID
                        $imgurl = DB::table('contactsliderimages')->where('id', $id)->pluck('imageurl');
                        $imgurl = urldecode($imgurl[0]);

                        // DELETE OLD IMAGE
                        if (File::exists(public_path() . $imgurl)) {
                            File::delete(public_path() . $imgurl);
                        }

                        // DELETE RECORD
                        DB::table('contactsliderimages')->where('id', '=', $id)->delete();
                        break;

                }

                return "success";

        }

        /* HOME */

        // EDIT HOME COPY
        public function doHomeCopyEdit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'homesection1header' => 'required',
            'homesection1text' => 'required',
            'homesection2header' => 'required',
            'homesection2text' => 'required',
            'homesection3header' => 'required',
            'homesection3text' => 'required',
            'homesection4text' => 'required',
            'homeourclientstext' => 'required',

            ]);

            $this->validate($request, $validateFields);

            $homesection1header = $request->homesection1header;

            $homesection1text = $request->homesection1text;

            $homesection2header = $request->homesection2header;

            $homesection2text = $request->homesection2text;

            $homesection3header = $request->homesection3header;

            $homesection3text = $request->homesection3text;

            $homesection4text = $request->homesection4text;

            $homeourclientstext = $request->homeourclientstext;

            $homesection1text = str_replace("*", "&bull;", $homesection1text);

            $homesection2text = str_replace("*", "&bull;", $homesection2text);

            $homesection3text = str_replace("*", "&bull;", $homesection3text);

            $homesection4text = str_replace("*", "&bull;", $homesection4text);

            
            // UPDATE RECORD
            DB::table('homecopy')
            ->where('id', '1')
            ->update([
                'homesection1header' => $homesection1header,
                'homesection1text' => $homesection1text,
                'homesection2header' => $homesection2header,
                'homesection2text' => $homesection2text,
                'homesection3header' => $homesection3header,
                'homesection3text' => $homesection3text,
                'homesection4text' => $homesection4text,
                'homeourclienttext' => $homeourclientstext,
            ]);

            return redirect("homecopy");
            
        }

        /* END HOME */

        /* ABOUT */

        // EDIT HOME COPY
        public function doAboutCopyEdit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'abouttext' => 'required',
            ]);

            $this->validate($request, $validateFields);

            $abouttext = $request->abouttext;

            $abouttext = str_replace("*", "&bull;", $abouttext);

            
            // UPDATE RECORD
            DB::table('abouttext')
            ->where('id', '1')
            ->update([
                'abouttext' => $abouttext,

            ]);

            return redirect("aboutcopy");
            
        }

        /* END ABOUT */

        /* AREAS OF PRACTICE */

            // EDIT AREAS OF PRACTICE COPY
            public function doAreasOfPracticeCopyEdit(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'ourwork1header' => 'required',
                'ourwork1text' => 'required',
                'ourwork2header' => 'required',
                'ourwork2text' => 'required',
                'ourwork3header' => 'required',
                'ourwork3text' => 'required',
                'ourwork4header' => 'required',
                'ourwork4text' => 'required',
                'ourworkadditionalheader' => 'required',
                'ourworkadditionaltext' => 'required',
                ]);

                $this->validate($request, $validateFields);

                $ourwork1header = $request->ourwork1header;

                $ourwork1text = $request->ourwork1text;

                $ourwork2header = $request->ourwork2header;

                $ourwork2text = $request->ourwork2text;

                $ourwork3header = $request->ourwork3header;

                $ourwork3text = $request->ourwork3text;

                $ourwork4header = $request->ourwork4header;

                $ourwork4text = $request->ourwork4text;

                $ourworkadditionalheader = $request->ourworkadditionalheader;

                $ourworkadditionaltext = $request->ourworkadditionaltext;

                $ourwork1text = str_replace("*", "&bull;", $ourwork1text);

                $ourwork2text = str_replace("*", "&bull;", $ourwork2text);

                $ourwork3text = str_replace("*", "&bull;", $ourwork3text);

                $ourwork4text = str_replace("*", "&bull;", $ourwork4text);

                $ourworkadditionaltext = str_replace("*", "&bull;", $ourworkadditionaltext);

                
                // UPDATE RECORD
                DB::table('areasofpracticecopy')
                ->where('id', '1')
                ->update([
                    'ourwork1header' => $ourwork1header,
                    'ourwork1text' => $ourwork1text,
                    'ourwork2header' => $ourwork2header,
                    'ourwork2text' => $ourwork2text,
                    'ourwork3header' => $ourwork3header,
                    'ourwork3text' => $ourwork3text,
                    'ourwork4header' => $ourwork4header,
                    'ourwork4text' => $ourwork4text,
                    'ourworkadditionalheader' => $ourworkadditionalheader,
                    'ourworkadditionaltext' => $ourworkadditionaltext,
                ]);

                return redirect("areasofpracticecopy");
                
            }

        /* END AREAS OF PRACTICE */

        /* SOLUTIONS */

        // ADD CASE STUDY 
        public function doCaseStudyAdd(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'image' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif,svg',
            'pdf' => 'required',
            'pdf' => 'mimes:pdf',
            'header' => 'required',
            'text' => 'required',
            'type' => 'required'
            ]);

            $this->validate($request, $validateFields);

            $header = $request->header;

            $text = $request->text;

            $type = $request->type;

            // GET IMAGE NAME
            $imageName = time() . $request->image->getClientOriginalName();

            // GET PDF NAME
            $pdfName = time() . $request->pdf->getClientOriginalName();

            switch($type) {
                case 'strategy':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/solutions/strategy'), $imageName);

                    $newImageURL = '/images/solutions/strategy/' . $imageName;

                    // UPLOAD PDF TO FOLDER
                    $upload = $request->pdf->move(public_path('/pdf/solutions/strategy'), $pdfName);

                    $newPDFURL = '/pdf/solutions/strategy/' . $pdfName;

                    DB::table('strategycasestudies')->insert( [
                        'header' => $header,
                        'text' => $text,
                        'pdfurl' => $newPDFURL,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'talentmanagement':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/solutions/talentmanagement'), $imageName);

                    $newImageURL = '/images/solutions/talentmanagement/' . $imageName;

                    // UPLOAD PDF TO FOLDER
                    $upload = $request->pdf->move(public_path('/pdf/solutions/talentmanagement'), $pdfName);

                    $newPDFURL = '/pdf/solutions/talentmanagement/' . $pdfName;

                    DB::table('talentmanagementcasestudies')->insert( [
                        'header' => $header,
                        'text' => $text,
                        'pdfurl' => $newPDFURL,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'leadereffectiveness':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/solutions/leadereffectiveness'), $imageName);

                    $newImageURL = '/images/solutions/leadereffectiveness/' . $imageName;

                    // UPLOAD PDF TO FOLDER
                    $upload = $request->pdf->move(public_path('/pdf/solutions/leadereffectiveness'), $pdfName);

                    $newPDFURL = '/pdf/solutions/leadereffectiveness/' . $pdfName;

                    DB::table('leadereffectivenesscasestudies')->insert( [
                        'header' => $header,
                        'text' => $text,
                        'pdfurl' => $newPDFURL,
                        'imageurl' => $newImageURL]
                    );
                    break;
                       

                case 'talentanalytics':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/solutions/talentanalytics'), $imageName);

                    $newImageURL = '/images/solutions/talentanalytics/' . $imageName;

                    // UPLOAD PDF TO FOLDER
                    $upload = $request->pdf->move(public_path('/pdf/solutions/talentanalytics'), $pdfName);

                    $newPDFURL = '/pdf/solutions/talentanalytics/' . $pdfName;

                    DB::table('talentanalyticscasestudies')->insert( [
                        'header' => $header,
                        'text' => $text,
                        'pdfurl' => $newPDFURL,
                        'imageurl' => $newImageURL]
                    );
                    break;

                case 'aboutbrochure':
                    // UPLOAD IMAGE TO FOLDER
                    $upload = $request->image->move(public_path('/images/about/brochures'), $imageName);

                    $newImageURL = '/images/about/brochures/' . $imageName;

                    // UPLOAD PDF TO FOLDER
                    $upload = $request->pdf->move(public_path('/pdf/about/brochures'), $pdfName);

                    $newPDFURL = '/pdf/about/brochures/' . $pdfName;

                    DB::table('aboutbrochures')->insert( [
                        'header' => $header,
                        'text' => $text,
                        'pdfurl' => $newPDFURL,
                        'imageurl' => $newImageURL]
                    );
                    break;
            }

            return "success";

        }

        // DELETE SLIDER IMAGE
        public function doCaseStudyDelete(Request $request) {
 
            // VALIDATE FIELDS
            $validateFields = ([
                'type' => 'required',
                'id' => 'required'
                ]);
    
            $this->validate($request, $validateFields);

            $id = $request->id;

            $type = $request->type;

            switch($type) {
                case 'strategy':
                    // GET RECORD WITH IMAGE ID
                    $imgurl = DB::table('strategycasestudies')->where('id', $id)->pluck('imageurl');
                    $imgurl = urldecode($imgurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl)) {
                        File::delete(public_path() . $imgurl);
                    }

                    // GET RECORD WITH PDF ID
                    $pdfurl = DB::table('strategycasestudies')->where('id', $id)->pluck('pdfurl');
                    $pdfurl = urldecode($pdfurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $pdfurl)) {
                        File::delete(public_path() . $pdfurl);
                    }

                    // DELETE RECORD
                    DB::table('strategycasestudies')->where('id', '=', $id)->delete();
                    break;

                case 'talentmanagement':
                    // GET RECORD WITH IMAGE ID
                    $imgurl = DB::table('talentmanagementcasestudies')->where('id', $id)->pluck('imageurl');
                    $imgurl = urldecode($imgurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl)) {
                        File::delete(public_path() . $imgurl);
                    }

                    // GET RECORD WITH IMAGE ID
                    $pdfurl = DB::table('talentmanagementcasestudies')->where('id', $id)->pluck('pdfurl');
                    $pdfurl = urldecode($pdfurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $pdfurl)) {
                        File::delete(public_path() . $pdfurl);
                    }

                    // DELETE RECORD
                    DB::table('talentmanagementcasestudies')->where('id', '=', $id)->delete();
                    break;

                case 'leadereffectiveness':
                    // GET RECORD WITH IMAGE ID
                    $imgurl = DB::table('leadereffectivenesscasestudies')->where('id', $id)->pluck('imageurl');
                    $imgurl = urldecode($imgurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl)) {
                        File::delete(public_path() . $imgurl);
                    }

                    // GET RECORD WITH IMAGE ID
                    $pdfurl = DB::table('leadereffectivenesscasestudies')->where('id', $id)->pluck('pdfurl');
                    $pdfurl = urldecode($pdfurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $pdfurl)) {
                        File::delete(public_path() . $pdfurl);
                    }

                    // DELETE RECORD
                    DB::table('leadereffectivenesscasestudies')->where('id', '=', $id)->delete();
                    break;

                case 'talentanalytics':
                    // GET RECORD WITH IMAGE ID
                    $imgurl = DB::table('talentanalyticscasestudies')->where('id', $id)->pluck('imageurl');
                    $imgurl = urldecode($imgurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl)) {
                        File::delete(public_path() . $imgurl);
                    }

                    // GET RECORD WITH IMAGE ID
                    $pdfurl = DB::table('talentanalyticscasestudies')->where('id', $id)->pluck('pdfurl');
                    $pdfurl = urldecode($pdfurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $pdfurl)) {
                        File::delete(public_path() . $pdfurl);
                    }

                    // DELETE RECORD
                    DB::table('talentanalyticscasestudies')->where('id', '=', $id)->delete();
                    break;

                case 'aboutbrochure':
                    // GET RECORD WITH IMAGE ID
                    $imgurl = DB::table('aboutbrochures')->where('id', $id)->pluck('imageurl');
                    $imgurl = urldecode($imgurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $imgurl)) {
                        File::delete(public_path() . $imgurl);
                    }

                    // GET RECORD WITH IMAGE ID
                    $pdfurl = DB::table('aboutbrochures')->where('id', $id)->pluck('pdfurl');
                    $pdfurl = urldecode($pdfurl[0]);

                    // DELETE OLD IMAGE
                    if (File::exists(public_path() . $pdfurl)) {
                        File::delete(public_path() . $pdfurl);
                    }

                    // DELETE RECORD
                    DB::table('aboutbrochures')->where('id', '=', $id)->delete();
                    break;

                }

                return "success";
                
        }   

            // EDIT STRATEGY COPY
            public function doStrategyCopyEdit(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'strategytext' => 'required',
                ]);

                $this->validate($request, $validateFields);

                $strategytext = $request->strategytext;

                $strategytext = str_replace("*", "&bull;", $strategytext);

                
                // UPDATE RECORD
                DB::table('strategytext')
                ->where('id', '1')
                ->update([
                    'strategytext' => $strategytext,

                ]);

                return redirect("strategycopy");
                
            }

            // EDIT TALENT MANAGEMENT COPY
            public function doTalentManagementCopyEdit(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'talentmanagementtext' => 'required',
                ]);

                $this->validate($request, $validateFields);

                $talentmanagementtext = $request->talentmanagementtext;

                $talentmanagementtext = str_replace("*", "&bull;", $talentmanagementtext);

                
                // UPDATE RECORD
                DB::table('talentmanagementtext')
                ->where('id', '1')
                ->update([
                    'talentmanagementtext' => $talentmanagementtext,

                ]);

                return redirect("talentmanagementcopy");
                
            }

            // EDIT LEADER EFFECTIVENESS COPY
            public function doLeaderEffectivenessCopyEdit(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'leadereffectivenesstext' => 'required',
                ]);

                $this->validate($request, $validateFields);

                $leadereffectivenesstext = $request->leadereffectivenesstext;

                $leadereffectivenesstext = str_replace("*", "&bull;", $leadereffectivenesstext);
                
                // UPDATE RECORD
                DB::table('leadereffectivenesstext')
                ->where('id', '1')
                ->update([
                    'leadereffectivenesstext' => $leadereffectivenesstext,

                ]);

                return redirect("leadereffectivenesscopy");
                
            }

            // EDIT TALENT ANALYTICS COPY
            public function doTalentAnalyticsCopyEdit(Request $request) {

                // VALIDATE FIELDS
                $validateFields = ([
                'talentanalyticstext' => 'required',
                ]);

                $this->validate($request, $validateFields);

                $talentanalyticstext = $request->talentanalyticstext;
                
                $talentanalyticstext = str_replace("*", "&bull;", $talentanalyticstext);

                
                // UPDATE RECORD
                DB::table('talentanalyticstext')
                ->where('id', '1')
                ->update([
                    'talentanalyticstext' => $talentanalyticstext,

                ]);

                return redirect("talentanalyticscopy");
                
            }

        /* END SOLUTIONS */

        /* CLIENTS */

        // ADD CLIENT
        public function doClientAdd(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'header' => 'required',
            'subheader' => 'required',
            ]);

            $this->validate($request, $validateFields);

            $header = $request->header;

            $subheader = $request->subheader;

            DB::table('clientslist')->insert( [
                'header' => $header,
                'subheader' => $subheader,]
            );

            return "success";

        }

        // DELETE CLIENT
        public function doClientDelete(Request $request) {

 
            // VALIDATE FIELDS
            $validateFields = ([
                'id' => 'required'
            ]);
    
            $this->validate($request, $validateFields);

            $id = $request->id;

            // DELETE RECORD
            DB::table('clientslist')->where('id', '=', $id)->delete();

            return "success";
                
        }   

        /* END CLIENTS */

        /* CONTACT */

        // EDIT HOME COPY
        public function doContactDetailsEdit(Request $request) {

            // VALIDATE FIELDS
            $validateFields = ([
            'contact1name' => 'required',
            'contact1number' => 'required',
            'contact1email' => 'required',
            'contact2name' => 'required',
            'contact2number' => 'required',
            'contact2email' => 'required',
            ]);

            $this->validate($request, $validateFields);

            $contact1name = $request->contact1name;

            $contact1number = $request->contact1number;

            $contact1email = $request->contact1email;

            $contact2name = $request->contact2name;

            $contact2number = $request->contact2number;

            $contact2email = $request->contact2email;
            
            // UPDATE RECORD
            DB::table('contactdetails')
            ->where('id', '1')
            ->update([
                'contact1name' => $contact1name,
                'contact1number' => $contact1number,
                'contact1email' => $contact1email,
                'contact2name' => $contact2name,
                'contact2number' => $contact2number,
                'contact2email' => $contact2email,
            ]);

            return redirect("contactdetails");
            
        }

        /* END CONTACT */

        public static function logout()
        {

            session()->flush();

            return redirect("login");
        }

    }

?>