<?php 

// GET HOME SLIDER IMAGES
$solutionsSliderImages = DB::table('solutionssliderimages')->get();

// GET HOME SLIDER IMAGES
$talentanalyticscopy = DB::table('talentanalyticstext')->get();

// GET CASE STUDIES
$talentanalyticscasestudies = DB::table('talentanalyticscasestudies')->orderBy('id', 'asc')->get();

$page = "Talent Intitute | Talent Analytics";

?>
@include('layouts.header')

<body>
@include('layouts.nav')
@include('layouts.loader')

<h1 style="display: none;">Talent Analytics</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($solutionsSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1" style="width: 100%; margin: 0; text-align: left;">

        <div id="homesection1body">

            <h5>Talent Analytics</h5>

            <div id="homesection1bodydivider"></diV>

            <h6><?php echo $talentanalyticscopy[0]->talentanalyticstext;?></h6>

        </div>

    </div>

<?php $counter = 0; ?>

<?php foreach ($talentanalyticscasestudies as $study) { ?>

<?php if ($counter % 2 != 1) { ?> 

    <div class="homeourclients" style="top: 0; margin: 50px auto;">

        <div class="homeourclientsbody">

                <h6><?php echo $study->header; ?></h6>

                <div class="homeourclientsbodydivider"></div>

                <p><?php echo $study->text; ?></p6>

                <br><br>

                <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" download>DOWNLOAD</a>

                <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" target="_blank">VIEW</a>

        </div>

        <div class="homeourclientsimage">

            <img src="<?php echo $study->imageurl; ?>">

        </div>

        <div style="clear: both;"></div>

    </div>

<?php } else { ?>

    <div class="homeourclients" style="top: 0; margin: 50px auto;">

        <div class="homeourclientsimage">

            <img src="<?php echo $study->imageurl; ?>">

        </div>

        <div class="homeourclientsbody">

                <h6><?php echo $study->header;?></h6>

                <div class="homeourclientsbodydivider"></div>

                <p><?php echo $study->text; ?></p6>

                <br><br>

                <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" download>DOWNLOAD</a>

                <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" target="_blank">VIEW</a>

        </div>

        <div style="clear: both;"></div>

    </div>

<?php } ?>

<?php $counter+= 1; ?>

<?php } ?>

@include('layouts.footer')