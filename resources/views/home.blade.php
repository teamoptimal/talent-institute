<?php 

// GET HOME SLIDER IMAGES
$homeSliderImages = DB::table('homesliderimages')->orderBy('id', 'asc')->get();

// GET HOME SLIDER IMAGES
$homeCopy = DB::table('homecopy')->get();

$page = "Talent Intitute | Strategic Talent Management Consulting";

?>
@include('layouts.header')

<body>
@include('layouts.loader')
@include('layouts.nav')

<h1 style="display: none;">Strategic Talent Management Consulting</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($homeSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                    <h4 class="sliderText" style="white-space: pre-wrap;"><?php echo $image->imagetext; ?><br><small>Where we are</small></h4>
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1">

        <div id="homesection1header">

            <h5>"See talent clearly. Manage it wisely."</h4>

        </div>

        <div id="homesection1body">

            <h5><?php echo $homeCopy[0]->homesection1header; ?></h5>

            <div id="homesection1bodydivider"></diV>

            <h6><?php echo $homeCopy[0]->homesection1text;?></h6>

        </div>

    </div>

    <div id="homesection2and3">

        <div id="homesection2areas">

            <h6>Areas of Practice</h6>

            <br>

            <a href="/strategy"><div id="homeareasstrategy" class="homeareasblock">

                <div id="homeareasstrategyheader" class="homeareasblockheader">
                    <img class="homeareasicon" src="{{asset('images/home/strategyicon.png')}}">
                    <p>Strategy</p>
                </div>

                <div id="homeareasstrategybody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgstrategy"></div>

                    <div id="homeareasstrategybodytext">

                        <p>Analyse</p>
                        <p>Design</p>
                        <p>Execute</p>

                    </div>

                </div>

            </div></a>

            <a href="/talentmanagement"><div id="homeareastalentmanagement" class="homeareasblock">

                <div id="homeareastalentmanagementheader" class="homeareasblockheader">
                    <img class="homeareasicon" src="{{asset('images/home/talentmanagementicon.png')}}">
                    <p>Talent Management Architecture</p>
                </div>

                

                <div id="homeareastalentmanagementbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgtalentmanagement"></div>

                    <div id="homeareastalentmanagementbodytext">

                        <p>Understand</p>
                        <p>Create</p>
                        <p>Integrate</p>

                    </div>

                </div>

            </div></a>

            <a href="/leadereffectiveness"><div id="homeareasleadereffectiveness" class="homeareasblock">

                <div id="homeareasleadereffectivenessheader" class="homeareasblockheader">
                    <img class="homeareasicon" src="{{asset('images/home/leadereffectivenessicon.png')}}">
                    <p>Leader Effectiveness</p>
                </div>

                <div id="homeareasleadereffectivenessbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgleadereffectiveness"></div>

                    <div id="homeareasleadereffectivenessbodytext">

                        <p>Define</p>
                        <p>Align</p>
                        <p>Invest</p>

                    </div>

                </div>

            </div></a>

            <a href="/talentanalytics"><div id="homeareastalentanalytics" class="homeareasblock">

                <div id="homeareastalentanalyticsheader" class="homeareasblockheader">
                    <img class="homeareasicon" src="{{asset('images/home/talentanalyticsicon.png')}}">
                    <p>Talent Analytics</p>
                </div>

                <div id="homeareastalentanalyticsbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgtalentanalytics"></div>

                    <div id="homeareastalentanalyticsbodytext">

                        <p>Measure</p>
                        <p>Interpret</p>
                        <p>Decide</p>

                    </div>

                </div>

            </div></a>

        </div>

        <div id="homesection2body">

                <p id="homesection2bodyheader"><?php echo $homeCopy[0]->homesection2header; ?></p>
                <p id="homesection2bodytext"><?php echo $homeCopy[0]->homesection2text; ?></p>

        </div>

        <div id="homesection3body">

            <p id="homesection3bodyheader"><?php echo $homeCopy[0]->homesection3header; ?></p>
            <p id="homesection3bodytext"><?php echo $homeCopy[0]->homesection3text; ?></p>

            <a href="/areasofpractice" class="readMoreButton">EXPLORE</a>

        </div>

        </div>

    </div>

    <div id="homesection4">

        <div id="homesection4body">

            <p><?php echo $homeCopy[0]->homesection4text; ?> </p>

            <div id="homesection4divider"></div>

        </div>

    </div>

    <div id="homeourclientsparent">

        <div id="homeourclients">

            <div id="homeourclientsimage">

                <img src="{{ asset('images/home/ourclients.jpg') }}">

            </div>

            <div id="homeourclientsbody">

                    <h6>Our Clients</h6>

                    <div id="homeourclientsbodydivider"></div>

                    <p><?php echo $homeCopy[0]->homeourclienttext;?></p6>

                    <br><br>

                    <a href="/clients" class="readMoreButton">VIEW</a>


            </div>

            <div style="clear: both;"></div>

        </div>

    </div>
  
 
@include('layouts.footer')