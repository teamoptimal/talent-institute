    <!-- jQuery -->
    <script src="{{asset('/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{asset('/vendors/nprogress/nprogress.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('/vendors/iCheck/icheck.min.js')}}"></script>
    <!-- Datatables -->
    <script src="{{asset('/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
    <script src="{{asset('/vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('/vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('/vendors/pdfmake/build/vfs_fonts.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('/js/custom.min.js')}}"></script>

     <!-- JQUERY PROMPT -->
    <script src="{{asset('/js/jquery.prompt.js')}}"></script>

    <!-- JQUERY CONFIRM -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

    <!-- CUSTOM SCRIPTS -->

    <!-- JQUERY PROMPTS -->
    <script>

      function addImage(type) {

        // INPUT NEW VALUES
        $.confirm({
          title: 'Add Slider Image',
          theme: "supervan",
          content: '' +
          '<form action="" class="formName" style="overflow: hidden;" id="formSliderImage">' +
          '<input type="hidden" name="_token" value="{{ csrf_token()}}">' +
          '<div class="form-group">' +
          '<label>Image Text</label>' +
          '<textarea cols="40" rows="1" class="textdata form-control" required>' +
          '</textarea>' +
          '</div> <br>' +
          '<div class="form-group">' +
          '<label>Image</label>' +
          '<input type="file" name="image" id="amenityImageInput" style="margin: 0 auto;" required>' + 
          '</div>' +
          '</form>',
          buttons: {
              // ON SUBMIT
              formSubmit: {
                  text: 'Add',
                  btnClass: 'btn-blue',
                  action: function () {

                    var data =  new FormData();
                    data.append('image', $('#amenityImageInput')[0].files[0]);
                    data.append('text', this.$content.find('.textdata').val());
                    data.append('type', type);

                      // DO WHATEVER OPERATION HERE
                      // AJAX OPERATION TO EDIT
                      var ajaxOp = $.ajax({
                        cache: false,
                        async: true,
                        url: "{{ url('/dosliderimageadd') }}",
                        method: 'post',
                        contentType: false,
                        processData: false,
                        data: data,
                        /*
                        data: {
                          "_token": "{{ csrf_token() }}",
                          id: reserveID,
                          url: imageURL,
                          image: $('#headerImageInput').prop('files')[0]
                        },
                        */
                        success: function(result) {
                          $.alert({
                            title: 'Image Added!',
                            content: '',
                            theme: 'supervan',
                            onClose: function () {
                              location.reload();
                            },
                          });
                          
                        },
                      });

                      
                  }
              },
              cancel: function () {
                  //close
              },
          },
          onContentReady: function () {
              // bind to events
              var jc = this;
              this.$content.find('form').on('submit', function (e) {
                  // if the user submits the form by pressing enter in the field.
                  e.preventDefault();
                  jc.$$formSubmit.trigger('click'); // reference the button and click it
              });
          }
        });

      };

      function deleteImage(type, id) {

         $.confirm({
            title: 'Delete Image?',
            theme: "supervan",
            content: 'This image will be removed',
            buttons: {
                confirm: function () {

                  // AJAX OPERATION TO EDIT
                  var ajaxOp = $.ajax({
                    url: "{{ url('/dosliderimagedelete') }}",
                    method: 'post',
                    data: {
                      "_token": "{{ csrf_token() }}",
                      type: type,
                      id: id,
                    },
                    /*
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: reserveID,
                      url: imageURL,
                      image: $('#headerImageInput').prop('files')[0]
                    },
                    */
                    success: function(result) {
                      $.alert({
                        title: 'Deleted!',
                        content: "",
                        theme: 'supervan',
                        onClose: function () {
                          location.reload();
                        },
                      });
                      
                    }
                  });
     
                },
                cancel: function () {
                 
                }
            }
          });
      }

      

      function addCaseStudy(type) {

        // INPUT NEW VALUES
        $.confirm({
          title: 'Add Brochure',
          theme: "supervan",
          content: '' +
          '<form action="" class="formName" style="overflow: hidden;" id="formSliderImage">' +
          '<input type="hidden" name="_token" value="{{ csrf_token()}}">' +
          '<div class="form-group">' +
          '<label>Case Study Header</label>' +
          '<textarea cols="40" rows="1" id="header" class="textdata form-control" required>' +
          '</textarea>' +
          '</div> <br>' +
          '<div class="form-group">' +
          '<label>Case Study Text</label>' +
          '<textarea cols="40" rows="5" id="text" class="textdata form-control" required>' +
          '</textarea>' +
          '</div> <br>' +
          '<div class="form-group">' +
          '<label>Image</label>' +
          '<input type="file" name="image" id="imageInput" style="margin: 0 auto;" required>' + 
          '</div> <br>' +
          '<div class="form-group">' +
          '<label>PDF</label>' +
          '<input type="file" name="pdf" id="pdfInput" style="margin: 0 auto;" required>' + 
          '</div>' +
          '</form>',
          buttons: {
              // ON SUBMIT
              formSubmit: {
                  text: 'Add',
                  btnClass: 'btn-blue',
                  action: function () {

                    var data =  new FormData();
                    data.append('image', $('#imageInput')[0].files[0]);
                    data.append('pdf', $('#pdfInput')[0].files[0]);
                    data.append('header', this.$content.find('#header').val());
                    data.append('text', this.$content.find('#text').val());
                    data.append('type', type);

                      // DO WHATEVER OPERATION HERE
                      // AJAX OPERATION TO EDIT
                      var ajaxOp = $.ajax({
                        cache: false,
                        async: true,
                        url: "{{ url('/docasestudyadd') }}",
                        method: 'post',
                        contentType: false,
                        processData: false,
                        data: data,
                        /*
                        data: {
                          "_token": "{{ csrf_token() }}",
                          id: reserveID,
                          url: imageURL,
                          image: $('#headerImageInput').prop('files')[0]
                        },
                        */
                        success: function(result) {
                          $.alert({
                            title: 'Case Study Added!',
                            content: '',
                            theme: 'supervan',
                            onClose: function () {
                              location.reload();
                            },
                          });
                          
                        },
                      });

                      
                  }
              },
              cancel: function () {
                  //close
              },
          },
          onContentReady: function () {
              // bind to events
              var jc = this;
              this.$content.find('form').on('submit', function (e) {
                  // if the user submits the form by pressing enter in the field.
                  e.preventDefault();
                  jc.$$formSubmit.trigger('click'); // reference the button and click it
              });
          }
        });

        };

        function deleteCaseStudy(type, id) {

          $.confirm({
            title: 'Delete Case Study?',
            theme: "supervan",
            content: 'This case study will be removed',
            buttons: {
                confirm: function () {

                  // AJAX OPERATION TO EDIT
                  var ajaxOp = $.ajax({
                    url: "{{ url('/docasestudydelete') }}",
                    method: 'post',
                    data: {
                      "_token": "{{ csrf_token() }}",
                      type: type,
                      id: id,
                    },
                    /*
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: reserveID,
                      url: imageURL,
                      image: $('#headerImageInput').prop('files')[0]
                    },
                    */
                    success: function(result) {
                      $.alert({
                        title: 'Deleted!',
                        content: "",
                        theme: 'supervan',
                        onClose: function () {
                          location.reload();
                        },
                      });
                      
                    }
                  });

                },
                cancel: function () {
                  
                }
            }
          });
          }

      /** HOME  **/

        /* HOME SLIDER IMAGES */

        /* END HOME SLIDER IMAGES */

      /** END HOME **/

      /** ABOUT **/

        /* ABOUT SLIDER IMAGES */

        /* END ABOUT SLIDER IMAGES */

      /** END ABOUT **/

      /** AREAS OF PRACTICE **/

        /* AREAS OF PRACTICE SLIDER IMAGES */

        /* END AREAS OF PRACTICE SLIDER IMAGES */

      /** END AREAS OF PRACTICE **/

      /** SOLUTIONS **/

        /* SOLUTIONS SLIDER IMAGES */

        /* END SOLUTIONS SLIDER IMAGES */

        /* STRATEGY */

        /* END STRATEGY */

        /* TALENT MANAGEMENT */

        /* END TALENT MANAGEMENT */

        /* LEADER EFFECTIVENESS */

        /* END LEADER EFFECTIVENESS */

        /* TALENT ANALYTICS */

        /* END TALENT ANALYTICS */

      /** END SOLUTIONS **/

      /** CLIENTS **/

      function addClient(type) {

        // INPUT NEW VALUES
        $.confirm({
          title: 'Add Client',
          theme: "supervan",
          content: '' +
          '<form action="" class="formName" style="overflow: hidden;" id="formSliderImage">' +
          '<input type="hidden" name="_token" value="{{ csrf_token()}}">' +
          '<div class="form-group">' +
          '<label>Case Study Header</label>' +
          '<textarea cols="40" rows="1" id="header" class="textdata form-control" required>' +
          '</textarea>' +
          '</div> <br>' +
          '<div class="form-group">' +
          '<label>Case Study Subheader</label>' +
          '<textarea cols="40" rows="2" id="subheader" class="textdata form-control" required>' +
          '</textarea>' +
          '</div> <br>' +
          '</form>',
          buttons: {
              // ON SUBMIT
              formSubmit: {
                  text: 'Add',
                  btnClass: 'btn-blue',
                  action: function () {

                    var data =  new FormData();
                    data.append('header', this.$content.find('#header').val());
                    data.append('subheader', this.$content.find('#subheader').val());

                      // DO WHATEVER OPERATION HERE
                      // AJAX OPERATION TO EDIT
                      var ajaxOp = $.ajax({
                        cache: false,
                        async: true,
                        url: "{{ url('/doclientadd') }}",
                        method: 'post',
                        contentType: false,
                        processData: false,
                        data: data,
                        /*
                        data: {
                          "_token": "{{ csrf_token() }}",
                          id: reserveID,
                          url: imageURL,
                          image: $('#headerImageInput').prop('files')[0]
                        },
                        */
                        success: function(result) {
                          $.alert({
                            title: 'Client Added!',
                            content: '',
                            theme: 'supervan',
                            onClose: function () {
                              location.reload();
                            },
                          });
                          
                        },
                      });

                      
                  }
              },
              cancel: function () {
                  //close
              },
          },
          onContentReady: function () {
              // bind to events
              var jc = this;
              this.$content.find('form').on('submit', function (e) {
                  // if the user submits the form by pressing enter in the field.
                  e.preventDefault();
                  jc.$$formSubmit.trigger('click'); // reference the button and click it
              });
          }
        });

        };

        function deleteClient(id) {

          $.confirm({
            title: 'Delete Client?',
            theme: "supervan",
            content: 'This client will be removed',
            buttons: {
                confirm: function () {

                  // AJAX OPERATION TO EDIT
                  var ajaxOp = $.ajax({
                    url: "{{ url('/doclientdelete') }}",
                    method: 'post',
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: id,
                    },
                    /*
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: reserveID,
                      url: imageURL,
                      image: $('#headerImageInput').prop('files')[0]
                    },
                    */
                    success: function(result) {
                      $.alert({
                        title: 'Deleted!',
                        content: "",
                        theme: 'supervan',
                        onClose: function () {
                          location.reload();
                        },
                      });
                      
                    }
                  });

                },
                cancel: function () {
                  
                }
            }
          });
          }

        function deleteCaseStudy(type, id) {

          $.confirm({
            title: 'Delete Case Study?',
            theme: "supervan",
            content: 'This case study will be removed',
            buttons: {
                confirm: function () {

                  // AJAX OPERATION TO EDIT
                  var ajaxOp = $.ajax({
                    url: "{{ url('/docasestudydelete') }}",
                    method: 'post',
                    data: {
                      "_token": "{{ csrf_token() }}",
                      type: type,
                      id: id,
                    },
                    /*
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: reserveID,
                      url: imageURL,
                      image: $('#headerImageInput').prop('files')[0]
                    },
                    */
                    success: function(result) {
                      $.alert({
                        title: 'Deleted!',
                        content: "",
                        theme: 'supervan',
                        onClose: function () {
                          location.reload();
                        },
                      });
                      
                    }
                  });

                },
                cancel: function () {
                  
                }
            }
          });
          }

        /* CLIENTS SLIDER IMAGES */

        /* END CLIENTS SLIDER IMAGES */

      /** END CLIENTS **/

    </script>

    <!-- END JQUERY PROMPTS -->

    <!-- ADD IMAGE ON CARD EDIT PAGE -->
    <script>

      var imagesCreated = 0;

      function addImageElementToCard(){

        imagesCreated++;

        // CREATE NEW FILE INPUT ELEMENT AND ADD TO PARENT
        var newImage = $("<div id='imgParent" + imagesCreated + "' class='form-group'><label class='control-label col-md-3 col-sm-3 col-xs-12' for='img' style='padding-top: 3px !important;'><i onclick='removeImageElement(" + imagesCreated + ")' class='fa fa-times fa-2x'></i><span class='required'></span></label><div class='col-md-6 col-sm-6 col-xs-12'><input type='file' id='img" + imagesCreated + "' name='img" + imagesCreated + "' required='required' class='form-control col-md-7 col-xs-12'></div></div>").appendTo("#imageParent");

        $("#imgCount").val( parseInt($("#imgCount").val(), 10) + 1);

      }

      // REMOVE SELECTED IMAGE
      function removeImageElement($image){

        $("#imgParent" + $image).remove();

        $("#imgCount").val( parseInt($("#imgCount").val(), 10) - 1);

      }

      // DELETE SELECTED CARD
      function deleteCardImage($idcard, $url) {

        $.ajax({
          method: 'POST', // Type of response and matches what we said in the route
          url: $url, // This is the url we gave in the route
          data: { 'idcard' : $idcard, _token: '{{csrf_token()}}' }, // a JSON object to send back
          success: function(response){ // What to do if we succeed
                $("#archivedImageParent" + $idcard).remove();
          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
      });

      }

    </script>

  </body>
</html>