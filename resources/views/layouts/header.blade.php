<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta http-equiv="Cache-Control" content="max-age=200" />

        <title><?php echo $page; ?></title>
        <meta name="title" content ="The Talent Institute">

        <!-- Fonts -->
        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- MAIN CSS FILE -->
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <!-- FLEX SLIDER -->
        <link rel="stylesheet" type="text/css" href="{{asset('css/flexslider.css')}}"/>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- HAMBURGER MENU -->
        <link rel="stylesheet" href="{{asset('css/hamburgericonmenu.css')}}" />
        
        <!-- GOOGLE CAPTCHA -->
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- JQUERY PROMPT -->
    <link rel="stylesheet" href="{{asset('/css/jquery.prompt.css')}}"/>
    <!-- JQUERY CONFIRM -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
        
    </head>