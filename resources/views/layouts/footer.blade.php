        <div id="hamburgericonmenuwrapper">
            <div id="hamburgerui">
                <ul>
                    <li><a href="#"><span id="navtoggler"></span></a></li>
                </ul>
            </div>
            <nav id="fullscreenmenu">
                <ul>
                    <li><a href="/"><nav>Home</nav></a></li>
                    <li><a href="/about"><nav>About</nav></a></li>
                    <li><a href="/areasofpractice"><nav>Areas of Practice</nav></a></li>
                    <li><a href="/strategy"><nav>Strategy</nav></a></li>
                    <li><a href="/talentmanagement"><nav>Talent Management</nav></a></li>
                    <li><a href="/leadereffectiveness"><nav>Leader Effectiveness</nav></a></li>
                    <li><a href="/talentanalytics"><nav>Talent Analytics</nav></a></li>
                    <li><a href="/clients"><nav>Clients</nav></a></li>
                    <li><a href="/contact"><nav>Contact</nav></a></li>
                </ul>
            </nav>
        </div>

        <div id="footer">

            <div id="footerTop">

                <div id="footerLeft">

                    <p id="footerLeftHeader">Contact Us</p>

                    <div id="footerLeft1">

                        <p class="footerLeftSubHeader">Errol van Staden</p>

                        <p>Business Psychologist | Managing Director</p>

                        <p>+27 83 637 0700</p>

                        <a href="mailto:errol@talentinstitute.ae" style="text-decoration: none; color: #ffffff;"><p>errol@talentinstitute.ae</p></a>

                    </div>

                    <div id="footerLeft2">

                        <p class="footerLeftSubHeader">Marlene Nell</p>

                        <p>Principal Consultant</p>

                        <p>+27 83 631 9577</p>

                        <a href="mailto:marlene@talentinstitute.co.za" style="text-decoration: none; color: #ffffff;"><p>marlene@talentinstitute.co.za</p></a>

                    </div>

                </div>

                <div id="footerRight">

                    <nav>

                        <ul>

                            <li><a href="/">Home</a></li>

                            <li><a href="/about">About</a></li>

                            <li><a href="/areasofpractice">Areas of Practice</a></li>

                            <li><a href="/strategy">Solutions</a></li>

                            <li><a href="/clients">Clients</a></li>

                            <li><a href="/contact">Contact</a></li>

                        </ul>

                    </nav>

                </div>

                <div id="footerBottomDivider"></div>

            </div>

            <div id="footerBottom">

                <p>Copyright <span id="currentYear">2018</span> | All rights reserved</p>

                <p style="margin-top: 20px;"><a href="https://www.optimalonline.co.za/" target="_blank">Powered by Optimal Online</a></p>

            </div>
           
        </div>
        
        <!-- JQUERY -->
        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- FLEXSLIDER -->
        <script src="{{asset('js/jquery.flexslider.js')}}"></script>

        <script src="{{asset('js/hamburgericonmenu.js')}}"></script>

           <!-- JQUERY PROMPT -->
    <script src="{{asset('/js/jquery.prompt.js')}}"></script>

<!-- JQUERY CONFIRM -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

        
        

        <script>

            window.addEventListener("load", function () {
                setTimeout(function () {
                    $('.loader-overlay').hide();
                    changeYear();
                }, 500);

            });

        </script>

        <script>
        // CHANGE YEAR IN FOOTER
            function changeYear() {
                var d = new Date();
                var n = d.getFullYear();
                document.getElementById("currentYear").innerHTML = n;
            }
        </script>

        <script>

            // this is the id of the form
            $("#contactFormForm").submit(function(e) {
                //$('#contactForm').fadeIn("300");

                // AJAX OPERATION TO EDIT
                var ajaxOp = $.ajax({
                    url: "{{ url('/submitcontact') }}",
                    method: 'post',
                    data: {
                      "_token": "{{ csrf_token() }}",
                      name: $('#contactForm-name').val(),
                      email: $("#contactForm-email").val(),
                      message: $("#contactForm-message").val(),
                      captcha: grecaptcha.getResponse()

                    },
                    /*
                    data: {
                      "_token": "{{ csrf_token() }}",
                      id: reserveID,
                      url: imageURL,
                      image: $('#headerImageInput').prop('files')[0]
                    },
                    */
                    success: function(result) {
                        if(result == "sent") {
                            $.alert({
                                title: '<span style="font-family: Futura Light;">Thank you for your message!</span>',
                                content: "",
                                theme: 'supervan',
                                onClose: function () {
                                    // Clear the form.
                                    $('#contactForm-name').val('');
                                    $('#contactForm-email').val('');
                                    $('#contactForm-message').val('');
                                },
                            });
                        }
                      
                    }
                  });
            });

            function closeContactPopup() {
                $('#contactComplete').hide();
            }

        </script>

        <script>

            function openExplore() {
                $('.explore-nav').fadeIn("300");
            }

            function closeExplore() {
                $('.explore-nav').fadeOut("300");
            };

        </script>

        <script>
             // Can also be used with $(document).ready()
             $(window).on('load' ,function() {
                $('.flexslider').flexslider({
                    animation: "slide",
                    touch: false,
                    controlNav: false,
                    directionNav: false,
                    slideshowSpeed: "5000",
                    randomize: false,
                    pauseOnHover: false,
                    smoothHeight: "true",
                });
            }); 
            
        </script>

    </body>
</html>