<div id="desktopNav">

    <a href="/"><image id="desktopNavLogo" src="{{ asset('images/homeicon.png') }}"></a>

    <div>

        <div class="socialIcons">

            <a href="https://www.linkedin.com/company/talent-institute" target="_blank"><img src="{{ asset('images/social1.png') }}"></a>

            <a href="https://businessgateways.com/talentinstitute" target="_blank"><img src="{{ asset('images/social2.png') }}"></a>

            <a href="https://businessgateways.com/jsrs_images/SiteVisit/jsrsCertificate/TALENT%20INSTITUTE%20FZ-LLC-001.pdf" target="_blank"><img style="width: 60px; margin-top: 25px; vertical-align: top;" src="{{ asset('images/jsrs.png') }}"></a>

        </div>

        <div class="navbar">
            <a href="/about">
                <h3 style="<?php echo ($page == "About" ? "color: #0e7f7e;" : ""); ?>">About</h3>
                <p>Who we are</p>
            </a>
            <a href="/areasofpractice">
                <h3 style="<?php echo ($page == "Areas of Practice" ? "color: #0e7f7e;" : ""); ?>">Areas of Practice</h3>
                <p>What we do</p>
            </a>
            <div class="dropdown">
                <button class="dropbtn">
                    <h3>Solutions</h3>
                    <p>What we offer</p>
                <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                    <a style="float: none; <?php echo ($page == "Strategy" ? "color: #0e7f7e;" : ""); ?>" href="/strategy">Strategy</a>
                    <a style="float: none; <?php echo ($page == "Talent Management" ? "color: #0e7f7e;" : ""); ?>" href="/talentmanagement">Talent Management</a>
                    <a style="float: none; <?php echo ($page == "Leader Effectiveness" ? "color: #0e7f7e;" : ""); ?>" href="/leadereffectiveness">Leader Effectiveness</a>
                    <a style="float: none; <?php echo ($page == "Talent Analytics" ? "color: #0e7f7e;" : ""); ?>" href="/talentanalytics">Talent Analytics</a>
                </div>
            </div> 
            <a href="/clients">
                <h3 style="<?php echo ($page == "Clients" ? "color: #0e7f7e;" : ""); ?>">Clients</h3>
                <p>Who are they</p>
            </a>
            <a href="/contact">
                <h3 style="<?php echo ($page == "Contact" ? "color: #0e7f7e;" : ""); ?>">Contact</h3>
                <p>Get in touch</p>
            </a>
            <a onclick="openExplore()" href="javascript:void(0)">
                <h3>Explore</h3>
                <p>Website overview</p>
            </a>
        </div>

    </div>

</div>

<?php 

// GET BROCHURES
$aboutbrochures = DB::table('aboutbrochures')->orderBy('id', 'asc')->get();

// GET BROCHURES
$strategybrochures = DB::table('strategycasestudies')->orderBy('id', 'asc')->get();

// GET BROCHURES
$talentmanbrochures = DB::table('talentmanagementcasestudies')->orderBy('id', 'asc')->get();

// GET BROCHURES
$leadeffecbrochures = DB::table('leadereffectivenesscasestudies')->orderBy('id', 'asc')->get();

// GET BROCHURES
$talentanalybrochures = DB::table('talentanalyticscasestudies')->orderBy('id', 'asc')->get();

?>

<div class="explore-nav">

    <img src="{{ asset('images/homeicon.png') }}">

    <div id="explore-left">

        <h3>TALENT INSTITUTE BROCHURES</h3>

        <?php foreach($aboutbrochures as $brochure) { ?>

            <div class="explore-section">
                <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

                <a href="<?php echo $brochure->pdfurl; ?>" target="_blank"><li><h4><?php echo $brochure->header; ?></h4></li></a>

                </ul>

            </div>

        <?php } ?>

        <div class="explore-section">

            <a href="/strategy"><h4>Strategy</h4></a>

            <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

                <?php foreach($strategybrochures as $brochure) { ?>
                
                    <a href="<?php echo $brochure->pdfurl; ?>" target="_blank"><li><?php echo $brochure->header; ?></li></a>
                
                <?php } ?>

            </ul>

        </div>

        <div class="explore-section">

            <a href="/talentmanagement"><h4>Talent Management Architecture</h4></a>

            <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

            <?php foreach($talentmanbrochures as $brochure) { ?>

                <a href="<?php echo $brochure->pdfurl; ?>" target="_blank"><li><?php echo $brochure->header; ?></li></a>

            <?php } ?>

            </ul>

        </div>

        <div class="explore-section">

            <a href="/leadereffectiveness"><h4>Leader Effectiveness</h4></a>

            <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

            <?php foreach($leadeffecbrochures as $brochure) { ?>

                <a href="<?php echo $brochure->pdfurl; ?>" target="_blank"><li><?php echo $brochure->header; ?></li></a>

            <?php } ?>

            </ul>

        </div>

        <div class="explore-section">

            <a href="/talentanalytics"><h4>Talent Analytics</h4></a>

            <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

            <?php foreach($talentanalybrochures as $brochure) { ?>

                <a href="<?php echo $brochure->pdfurl; ?>" target="_blank"><li><?php echo $brochure->header; ?></li></a>

            <?php } ?>

            </ul>

        </div>

    </div>

    <div id="explore-right">

        <h3>EXPLORE</h3>

        <div class="explore-section">

            <a href="/about"><h4>About - Who we are</h4></a>

        </div>

        <div class="explore-section">

            <a href="/areasofpractice"><h4>Areas of Practice - What we do</h4></a>

        </div>

        <div class="explore-section">

            <h4>Solutions - What we offer</h4>

            <ul style="list-style: initial; list-style-position: outside; margin-left: 10px;">

                <a href="/strategy"><li>Strategy</li></a>

                <a href="/talentmanagement"><li>Talent Management Architecture</li></a>

                <a href="/leadereffectiveness"><li>Leader Effectiveness</li></a>

                <a href="/talentanalytics"><li>Talent Analytics</li></a>

            </ul>

        </div>

        <div class="explore-section">

            <a href="/clients"><h4>Clients - Who are they</h4></a>

        </div>

            <div class="explore-section">

            <a href="/contact"><h4>Contact - Get in Touch</h4></a>

        </div>
        

    </div>

    <a href="javascript:void(0)" onclick="closeExplore()"><div id="explore-close">

        X CLOSE

    </div></a>
    
</div>