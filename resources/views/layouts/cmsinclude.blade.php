<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="/" class="site_title"><span>The Talent Institute CMS</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span></span>
                <h2><?php echo session('name') . " " . session('surname'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                
                <h3>CMS</h3>
                <ul class="nav side-menu">

                    <li><a><i class="fa fa-image"></i> Home <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/homesliderimages'>Home Slider Images</a>
                            </li>
                            <li><a href='/homecopy'>Home Copy</a>
                            </li>
                        </li>
                      </ul>
                    </li>    

                    <li><a><i class="fa fa-image"></i> About <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/aboutsliderimages'>About Slider Images</a>
                            </li>
                            <li><a href='/aboutbrochures'>About Brochures</a>
                            </li>
                            <li><a href='/aboutcopy'>About Copy</a>
                            </li>
                        </li>
                      </ul>
                    </li>  

                    <li><a><i class="fa fa-image"></i> Areas of Practice <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/areasofpracticesliderimages'>Areas of Practice Slider Images</a>
                            </li>
                            <li><a href='/areasofpracticecopy'>Areas of Practice Copy</a>
                            </li>
                        </li>
                      </ul>
                    </li>  
                    
                    <li><a><i class="fa fa-image"></i> Solutions <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/solutionssliderimages'>Solutions Slider Images</a>
                            </li>
                            <li><a>Strategy<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="">
                                    <li><a href="/strategycopy">Page Text</a>
                                    </li>
                                    <li><a href="/strategycasestudies">Case Studies</a>
                                    </li>
                                </ul>
                            </li> 

                            <li><a>Talent Management<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="">
                                    <li><a href="/talentmanagementcopy">Page Text</a>
                                    </li>
                                    <li><a href="/talentmanagementcasestudies">Case Studies</a>
                                    </li>
                                </ul>
                            </li> 

                            <li><a>Leader Effectiveness<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="">
                                    <li><a href="/leadereffectivenesscopy">Page Text</a>
                                    </li>
                                    <li><a href="/leadereffectivenesscasestudies">Case Studies</a>
                                    </li>
                                </ul>
                            </li> 

                            <li><a>Talent Analytics<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu" style="">
                                    <li><a href="/talentanalyticscopy">Page Text</a>
                                    </li>
                                    <li><a href="/talentanalyticscasestudies">Case Studies</a>
                                    </li>
                                </ul>
                            </li> 
                        </li>
                      </ul>
                    </li> 

                    <li><a><i class="fa fa-image"></i> Clients <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/clientssliderimages'>Clients Slider Images</a>
                            </li>
                            <li><a href='/clientslist'>Clients List</a>
                            </li>
                        </li>
                      </ul>
                    </li>  

                    <li><a><i class="fa fa-image"></i> Contact <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                            <li><a href='/contactsliderimages'>Contact Slider Images</a>
                            </li>
                            <li><a href='/contactdetails'>Contact Details</a>
                            </li>
                        </li>
                      </ul>
                    </li>  

                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Website" href="/">
                <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Home" href="/cmshome">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="/dologout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo session('name') . " " . session('surname'); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="/dologout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->