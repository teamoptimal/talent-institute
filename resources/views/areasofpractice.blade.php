<?php 

// GET HOME SLIDER IMAGES
$areasOfPracticeSliderImages = DB::table('areasofpracticesliderimages')->get();

// GET HOME SLIDER IMAGES
$areasOfPracticeCopy = DB::table('areasofpracticecopy')->get();

$page = "Talent Institute | Areas of Practice";

?>
@include('layouts.header')

<body>
@include('layouts.nav')
@include('layouts.loader')

<h1 style="display: none;">Areas of Practice</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($areasOfPracticeSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1" style="width: 100%; margin: 0; text-align: left;">

        <div id="homesection1body" style="padding: 30px 0;">

            <h5>Our Work</h5>

            <div id="homesection1bodydivider"></div>

            <div id="ourworksection1" style="text-align: left;">

                <div class="ourworksectionnum"><p>1</p></div>

                <div class="ourworksectiontext">

                    <h6 style="padding: 0;"><?php echo $areasOfPracticeCopy[0]->ourwork1header; ?></h6>

                    <p style="white-space: pre-wrap;"><?php echo $areasOfPracticeCopy[0]->ourwork1text; ?></p>

                </div>

            </div>

            <div id="ourworksection2" style="text-align: left; background-color: #e7e7e8;">

                <div class="ourworksectionnum"><p>2</p></div>

                <div class="ourworksectiontext">

                    <h6 style="padding: 0;"><?php echo $areasOfPracticeCopy[0]->ourwork2header; ?></h6>

                    <p style="white-space: pre-wrap;"><?php echo $areasOfPracticeCopy[0]->ourwork2text; ?></p>

                </div>

            
            </div>

            <div id="ourworksection3" style="text-align: left;">

                <div class="ourworksectionnum"><p>3</p></div>

                <div class="ourworksectiontext">

                    <h6 style="padding: 0;"><?php echo $areasOfPracticeCopy[0]->ourwork3header; ?></h6>

                    <p style="white-space: pre-wrap;"><?php echo $areasOfPracticeCopy[0]->ourwork3text; ?></p>

                </div>

                        
            </div>

            <div id="ourworksection4" style="text-align: left; background-color: #e7e7e8;">

                <div class="ourworksectionnum"><p>4</p></div>

                <div class="ourworksectiontext">

                    <h6 style="padding: 0;"><?php echo $areasOfPracticeCopy[0]->ourwork4header; ?></h6>

                    <p style="white-space: pre-wrap;"><?php echo $areasOfPracticeCopy[0]->ourwork4text; ?></p>

                </div>

                        
            </div>

            <h6 style="text-align: center; font-size: 18px; font-weight: bold; margin-top: 40px; line-height: 24px;"><?php echo $areasOfPracticeCopy[0]->ourworkadditionalheader;?></h6>
            <p style="white-space: pre-wrap; font-weight: bold; margin-bottom: 40px; line-height: 24px;"><?php echo $areasOfPracticeCopy[0]->ourworkadditionaltext;?></p>

        </div>

    </div>

    <div id="homesection2and3" style="background: #ffffff;">

        <div id="homesection2areas">

            <a href="/strategy"><div id="homeareasstrategy" class="homeareasblock">

                <div id="homeareasstrategybody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgstrategy"></div>

                    <div id="homeareasstrategybodytext">

                        <p>Analyse</p>
                        <p>Design</p>
                        <p>Execute</p>

                    </div>

                </div>

            </div></a>

            <a href="/talentmanagement"><div id="homeareastalentmanagement" class="homeareasblock">    

                <div id="homeareastalentmanagementbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgtalentmanagement"></div>

                    <div id="homeareastalentmanagementbodytext">

                        <p>Understand</p>
                        <p>Create</p>
                        <p>Integrate</p>

                    </div>

                </div>

            </div></a>

            <a href="/leadereffectiveness"><div id="homeareasleadereffectiveness" class="homeareasblock">

                <div id="homeareasleadereffectivenessbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgleadereffectiveness"></div>

                    <div id="homeareasleadereffectivenessbodytext">

                        <p>Define</p>
                        <p>Align</p>
                        <p>Invest</p>

                    </div>

                </div>

            </div></a>

            <a href="/talentanalytics"><div id="homeareastalentanalytics" class="homeareasblock">

                <div id="homeareastalentanalyticsbody" class="homeareasblockimage">

                    <div class="homeareasblockimagebg" id="homeareasblockimagebgtalentanalytics"></div>

                    <div id="homeareastalentanalyticsbodytext">

                        <p>Measure</p>
                        <p>Interpret</p>
                        <p>Decide</p>

                    </div>

                </div>

            </div></a>

        </div>


    </div>
  
 
@include('layouts.footer')