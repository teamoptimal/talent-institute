<?php 

// GET HOME SLIDER IMAGES
$clientsSliderImages = DB::table('clientssliderimages')->get();

// GET CLIENTS LIST
$clients = DB::table('clientslist')->get();

$page = "Talent Intitute | Our Clients";

?>
@include('layouts.header')

<body>
@include('layouts.nav')
@include('layouts.loader')

<h1 style="display: none;">Our Clients</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($clientsSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1" style="width: 100%; margin: 0; text-align: left;">

        <div id="homesection1body">

            <h5>Our Clients</h5>

            <div id="homesection1bodydivider"></diV>

            <h6 style="text-align: center;">Some of the larger clients we have done work for include:</h6>

            <div id="clientsList">

                <?php $counter = 0; ?>

                <?php foreach ($clients as $c) { ?>

                    <div class="clientEntry">

                        <h6 style="padding: 0;"><?php echo $c->header; ?></h6>

                        <p><?php echo $c->subheader;?> </p>

                    </div>

                    <?php 
                    
                    $counter ++;

                    if ($counter == 3) { ?>

                        <br>

                    <?php $counter = 0; ?>

                    <?php } ?>
                
                <?php } ?>

            </div>

        </div>

    </div>
  
 
@include('layouts.footer')