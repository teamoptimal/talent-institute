<?php 

// GET HOME SLIDER IMAGES
$contactSliderImages = DB::table('contactsliderimages')->get();

// GET HOME SLIDER IMAGES
$homeCopy = DB::table('homecopy')->get();

// GET HEADER RECORD
$information = DB::table('contactdetails')->where('id', '1')->get();

$page = "Talent Intitute | Contact Us";

?>
@include('layouts.header')

<body>
@include('layouts.loader')
@include('layouts.nav')

<h1 style="display: none;">Contact Us</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($contactSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1" style="width: 100%; margin: 0; text-align: left; background-color: #ffffff;">

        <img id="contactImage" src="{{asset('images/globe.jpg')}}">

        <div id="homesection1body" style="background-color: initial; position: relative; z-index: 2;">

            <h5 style="z-index: 2;">Start a Conversation</h5>

            <div style="z-index: 2;" id="homesection1bodydivider"></diV>

            <h6 style="z-index: 2;">We would like to have a conversation with you about your specific needs. We hope that it will evolve in a working relationship, but in any event, we are confident that we can offer you the benefit of sharing experience, and we know we will benefit from learning about you, your context and your challenges.</h6>

            <br>

            <h6 style="z-index: 2;">If you would like to learn more about our solutions, please contact:</h6>
            

            <div id="contactBody">

                <div style="z-index: 2;" id="contactDetails">

                    <h6 style="z-index: 2;"><?php echo $information[0]->contact1name; ?></h6>

                    <p style="z-index: 2; margin-bottom: 8px;"><?php echo $information[0]->contact1number; ?></p>

                    <a style="z-index: 2;"><p><a style="text-decoration: none; color: #000000; unicode-bidi: bidi-override; direction: rtl;" href='mailto:<?php echo $information[0]->contact1email; ?>'><?php echo strrev($information[0]->contact1email); ?></a></p></a>

                    <h6 style="z-index: 2; margin-top:30px;"><?php echo $information[0]->contact2name; ?></h6>

                    <p style="z-index: 2; margin-bottom: 8px;"><?php echo $information[0]->contact2number; ?></p>

                    <a style="z-index: 2;"><p><a style="text-decoration: none; color: #000000; unicode-bidi: bidi-override; direction: rtl;" href='mailto:<?php echo $information[0]->contact2email; ?>'><?php echo strrev($information[0]->contact2email); ?></a>

                </div>

                <div id="contactForm" style="z-index: 2;">

                    <form id="contactFormForm" method="post" action="javascript:void(0);">

                        {{csrf_field()}}

                        <label>Fill out the form</label>

                        <input type="text" name="name" id="contactForm-name" placeholder="Name" required style="margin: 20px 10px 20px 0;"></input>

                        <input type="email" name="email" id="contactForm-email" placeholder="Email" required style="margin: 20px 0 20px 0;"></input>

                        <br>

                        <textarea rows="4" name="usermessage" id="contactForm-message" placeholder="Message" required></textarea>

                        <br>

                        <div class="g-recaptcha" data-sitekey="6LcGQYgUAAAAADa7TccY7_IBi2HuTFXuk6e4lCfH"></div>

                        <button style="margin:20px 0 20px 0;" type="submit" value="SUBMIT" class="btnContact readMoreButton">SEND</button>

                    </form>

                </div>


            </div>

        </div>

    </div>
 
@include('layouts.footer')