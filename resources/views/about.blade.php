<?php 

// GET HOME SLIDER IMAGES
$aaboutSliderImages = DB::table('aboutsliderimages')->get();

// GET HOME SLIDER IMAGES
$aboutCopy = DB::table('abouttext')->get();

$page = "Talent Institute | About | Strategic Talent Management Consulting";

// GET CASE STUDIES
$aboutBrochures = DB::table('aboutbrochures')->orderBy('id', 'asc')->get();

?>
@include('layouts.header')

<body>
@include('layouts.loader')
@include('layouts.nav')

<h1 style="display: none;">About</h1>

    <!-- Place somewhere in the <body> of your page -->
    <div class="flexslider flexsliderheader">
        <ul class="slides">
            <?php foreach ($aaboutSliderImages as $image) { ?>
                <li class="flexsliderheaderimageparent">
                    <img class="flexsliderheaderimage" src="<?php echo $image->imageurl; ?>" alt="<?php echo $image->imagetext; ?>" />
                </li>
            <?php } ?>
        </ul>
    </div>

    <div id="homesection1" style="width: 100%; margin: 0; text-align: left;">

        <div id="homesection1body">

            <h5>About</h5>

            <div id="homesection1bodydivider"></diV>

            <h6><?php echo $aboutCopy[0]->abouttext;?></h6>

            <a href="/areasofpractice" class="readMoreButton">READ MORE</a>

        </div>

    </div>

    <?php $counter = 0; ?>

    <?php foreach ($aboutBrochures as $study) { ?>

        <?php if ($counter % 2 != 1) { ?> 

            <div class="homeourclients" style="top: 0; margin: 50px auto;">

                <div class="homeourclientsbody">

                        <h6><?php echo $study->header; ?></h6>

                        <div class="homeourclientsbodydivider"></div>

                        <p><?php echo $study->text; ?></p6>

                        <br><br>

                        <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" download>DOWNLOAD</a>

                        <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" target="_blank">VIEW</a>

                </div>

                <div class="homeourclientsimage">

                    <img src="<?php echo $study->imageurl; ?>">

                </div>

                <div style="clear: both;"></div>

            </div>

            <?php } else { ?>

            <div class="homeourclients" style="top: 0; margin: 50px auto;">

                <div class="homeourclientsimage">

                    <img src="<?php echo $study->imageurl; ?>">

                </div>

                <div class="homeourclientsbody">

                        <h6><?php echo $study->header; ?></h6>

                        <div class="homeourclientsbodydivider"></div>

                        <p><?php echo $study->text; ?></p6>

                        <br><br>

                        <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" download>DOWNLOAD</a>

                        <a href="<?php echo $study->pdfurl;?>" class="readMoreButton" target="_blank">VIEW</a>

                </div>

                <div style="clear: both;"></div>

            </div>

    <?php } ?>

    <?php $counter++; ?>

    <?php } ?>
  
@include('layouts.footer')