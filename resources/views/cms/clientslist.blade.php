<?php 

$pageTitle = "Clients List";

// GET HEADER RECORD
$information = DB::table('clientslist')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Clients List<small> View </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <a href="javascript:void(0);" onclick="addClient('')" class="btn btn-success" style="margin: 20px 0!important; margin-left: 20px !important;">Add Client</a>
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/" enctype="multipart/form-data">

                    {{csrf_field()}}

                    @foreach ($information as $client)

                        <div class="col-md-55" id="client{{ $client->id }}">
                            <div class="thumbnail" style="margin: 0 !important;">
                                <div class="image view view-first">
                                    <img style="width: 100%; display: block;" id="clientSrc{{ $client->id }}" src='{{asset("images/client.jpg")}}'/>
                                    <div class="mask">
                                        <p> <br> </p>
                                        <div class="tools tools-bottom">
                                            
                                            <a href="javascript:void(0);" onclick="deleteClient({{ $client->id }})"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p style="text-align: center;"><b>{{ $client->header }}</b></p>
                                    <p style="text-align: center;">{{ $client->subheader }}</p>
                                </div>
                            </div>
                        </div>

                    @endforeach

                    <div class="ln_solid"></div>

                </form>
                </div>
            </div>
        </div>

    </div>
    

@extends('layouts.cmsfooter')
