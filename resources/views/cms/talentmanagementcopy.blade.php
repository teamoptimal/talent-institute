<?php 

$pageTitle = "Talent Management Copy Edit";

// GET HEADER RECORD
$information = DB::table('talentmanagementtext')->where('id', '1')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>Talent Management Copy<small> Edit </small></h2>
                
                <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <br>
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/dotalentmanagementcopyedit" enctype="multipart/form-data">

                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Talent Management Text <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" rows="30" name="talentmanagementtext" required="required"><?php echo $information[0]->talentmanagementtext; ?></textarea>
                        </div>
                    </div>


                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>

    </div>
    

@extends('layouts.cmsfooter')
