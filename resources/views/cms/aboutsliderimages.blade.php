<?php 

$pageTitle = "About Slider Images";

// GET HEADER RECORD
$information = DB::table('aboutsliderimages')->get();

?>

@extends('layouts.cmsheader')

@include('layouts.cmsinclude')

    <!-- Page Content -->
    <div class="right_col" style="min-height: 100% !important;">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                <h2>About Slider Images<small> View </small></h2>
                
                <div class="clearfix"></div>
                
                </div>
 
                <div class="x_content">

                <a href="javascript:void(0);" onclick="addImage('about')" class="btn btn-success" style="margin: 20px 0!important; margin-left: 20px !important;">Add Image</a>

                <br>
                
                <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" method="post" action="/" enctype="multipart/form-data">

                    {{csrf_field()}}

                    @foreach ($information as $image)

                        <div class="col-md-55" id="image{{ $image->id }}" data-text="{{ $image->imagetext }}" data-url="{{ $image->imageurl }}">
                            <div class="thumbnail" style="margin: 0 !important;">
                                <div class="image view view-first">
                                    <img style="width: 100%; display: block;" id="imageSrc{{ $image->id }}" src='{{ $image->imageurl }}' alt="{{ $image->imagetext }}}" />
                                    <div class="mask">
                                        <p> <br> </p>
                                        <div class="tools tools-bottom">
                                            
                                            <a href="javascript:void(0);" onclick="deleteImage('about', {{ $image->id }})"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="caption">
                                    <p style="text-align: center;" id="imagetext{{ $image->id }}">{{ $image->imagetext }}</p>
                                </div>
                            </div>
                        </div>

                    @endforeach

                    <div class="ln_solid"></div>

                </form>
                </div>
            </div>
        </div>

    </div>
    

@extends('layouts.cmsfooter')
