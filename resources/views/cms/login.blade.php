
<?php 

$page = "CMS Login";

?>
@include('layouts.header')

<body>
@include('layouts.loader')
@include('layouts.nav')
                <div class="login-header">CMS LOGIN</div>

                <div class="login-body">
                    <form method="POST" action="/dologin">

                        <meta name="csrf-token" content="{{ csrf_token() }}" />

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <br>

                        <label>Username</label> <br>
                        <input type="text" name="username" value="" autofocus> <br> <br>

                        <label>Password</label> <br>
                        <input type="password" name="password" value=""> <br> <br>

                        <input class="login-submit" type="submit" value="LOGIN">
                        
                    </form>
                </div>
             
@include('layouts.footer')
